import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {PostlistComponent} from './components/postlist/postlist.component';
import {PostComponent} from './components/post/post.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'posts',
    pathMatch: 'full'
  },
  {
    path: 'posts',
    component: PostlistComponent
  },
  {
    path: 'post/:postId',
    component: PostComponent
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
})

export class AppRoutingModule {}
