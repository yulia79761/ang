import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../services/users.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  postData: any;

  comments = [];

  constructor(private data: UsersService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.data.getPostById(params.postId)
        .subscribe(postData => this.postData = postData, err => console.log(err));
      this.data.getPostCommentsById(params.postId)
        .subscribe((postComments: any[]) => {
          this.comments = postComments;
        }, err => console.log(err));
    });
  }

}
