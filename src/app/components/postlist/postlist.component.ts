import {Component, OnDestroy, OnInit} from '@angular/core';
import {UsersService} from '../../services/users.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-postlist',
  templateUrl: './postlist.component.html',
  styleUrls: ['./postlist.component.css']
})
export class PostlistComponent implements OnInit, OnDestroy {

  postList: any[];
  componentAlive = new Subject();

  constructor(private data: UsersService) {
  }

  ngOnInit() {
    this.data.getPostList()
      .pipe(takeUntil(this.componentAlive))
      .subscribe(
      (data: any[]) => {
        this.postList = data;
        this.postList.forEach((post, index) => {
          this.data.getPostCommentsById(post.id)
            .subscribe((users: any[]) => this.postList[index].commentCount = data.length);
        });
      },
      err => {
        console.log(err);
        this.postList = [];
      });
  }

  ngOnDestroy() {
    this.componentAlive.next();
  }

}
