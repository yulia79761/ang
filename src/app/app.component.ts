import { Component, OnInit} from '@angular/core';
import { UsersService } from './services/users.service';
import { Users, User, UserId, UserComments } from './user';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [UsersService]
})
export class AppComponent implements OnInit {
  users: User[];

  constructor(private httpService: UsersService) {}
  ngOnInit() {
    this.getUserPost();
  }

  getUserPost() {
    this.httpService.getUser().subscribe((x: User[]) => {
      this.users = x;
    },
    err => {
      console.log('Error occured.');
    });
  }

}



