export class Users {
    title: string;
    body: string;
    userId: number;
}

export class User {
    title: string;
    body: string;
    userId: number;
}

export class UserComments {
    title: string;
    body: string;
    userId: number;
}

export class UserId {
    title: string;
    body: string;
    userId: number;
}