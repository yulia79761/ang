import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment';
import {take} from 'rxjs/operators';

import {Users, User, UserId, UserComments} from './../user';

@Injectable({
  providedIn: 'root'
})

export class UsersService {


  constructor(private http: HttpClient) { }

  getPostList() {
    return this.http.get(`${environment.apiUrl}/posts`).pipe(take(1));
  }

  getPostById(id: number | string) {
    return this.http.get(`${environment.apiUrl}/posts/${id}`).pipe(take(1));
  }

  getPostCommentsById(id: number | string) {
    return this.http.get(`${environment.apiUrl}/posts/${id}/comments`).pipe(take(1));
  }




  getUsers() {
    return this.http.get<Users>(`${environment.apiUrl}/posts`);
  }

  getUser() {
    return this.http.get<User[]>(`${environment.apiUrl}/posts`);
  }

  getUserComments(id: number | string) {
    return this.http.get<UserComments>(`${environment.apiUrl}/posts/${id}/comments`).pipe(take(1));
  }

  getUserId(id: number | string) {
    return this.http.get<UserId>(`${environment.apiUrl}/posts/${id}`).pipe(take(1));
  }
}
